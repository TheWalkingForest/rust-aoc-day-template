use day_{{ day }}::part2::process;

fn main() {
    let input = include_str!("input.txt");
    let result = process(input);

    println!("{result}");
}
