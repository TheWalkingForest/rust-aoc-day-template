# Rust Advent of Code Day Template

## License

The code in this repository is licensed under the [BSD-3-Clause](https://gitlab.com/TheWalkingForest/rust-aoc-day-template/-/blob/main/LICENSE) license, unless otherwise specified
